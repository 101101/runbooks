groups:
- name: GitLab Component Apdex Scores
  interval: 1m
  rules:
  ######################
  # Aggregation Stage
  ######################

  # Aggregate over all components within a service using a weighted average
  # Why use the avg_over_time[10m] value for the weight? This is because
  # if a component suddenly stops working, its RPS will drop to zero and
  # therefore its weight will also drop to zero, meaning that it won't affect
  # the apdex. Using a 10m average is an attempt at resolving this.
  # Note: we only include current components. This prevents apdex from dropping
  # when a component is removed from the metrics-catalog
  - record: gitlab_service_apdex:ratio
    expr: >
      sum by (environment, tier, type, stage) (
        ((gitlab_component_apdex:ratio >= 0) * (avg_over_time(gitlab_component_apdex:weight:score[10m]) >= 0))
        and on (tier, type, component)
        gitlab_component_service:mapping
      )
      /
      sum by (environment, tier, type, stage) (
        (avg_over_time(gitlab_component_apdex:weight:score[10m]) >= 0)
        and on (tier, type, component)
        gitlab_component_service:mapping
      )

  # Node-level aggregation
  - record: gitlab_service_node_apdex:ratio
    expr: >
      sum by (environment, tier, type, stage, shard, fqdn) ((gitlab_component_node_apdex:ratio >= 0) * (avg_over_time(gitlab_component_node_apdex:weight:score[10m]) >= 0))
      /
      sum by (environment, tier, type, stage, shard, fqdn) (avg_over_time(gitlab_component_node_apdex:weight:score[10m]) >= 0)

# These should be deprecated but are currently used by the general triage dashboard
- name: GitLab Service Apdex Score Stats
  interval: 5m
  rules:
  # Average values for each service, over a week
  - record: gitlab_service_apdex:ratio:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_service_apdex:ratio[1w])
  # Stddev for each service, over a week
  - record: gitlab_service_apdex:ratio:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_service_apdex:ratio[1w])

